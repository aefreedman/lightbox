﻿using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float Speed;

    private void Start()
    {
    }

    private void Update()
    {
        gameObject.transform.Rotate(Vector3.up, Speed, Space.World);
    }
}