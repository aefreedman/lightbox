﻿using UnityEngine;
using UnityEngine.UI;

public class RotationSlider : MonoBehaviour
{

    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(Callback);
        //_slider.value = Camera.main.GetComponent<Tonemapping>().exposureAdjustment / 8f;
    }
    private void Start()
    {
    }

    private void Update()
    {
    }

    private void Callback(float value)
    {
        Controller.Instance.SetRotation(value);
    }
}