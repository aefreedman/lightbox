﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class TemperatureSlider : MonoBehaviour {

    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(Callback);
        //_slider.value = Camera.main.GetComponent<Tonemapping>().exposureAdjustment / 8f;
    }

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void Callback(float value)
    {
        Controller.Instance.CurrentLightGroup.SetTemperature(MapSlider(value));
    }

    private static float MapSlider(float value)
    {
        const float min = 1500f;
        const float max = 12000f;
        var range = max - min;
        var result = value * range + min;
        return result;;
    }
}
