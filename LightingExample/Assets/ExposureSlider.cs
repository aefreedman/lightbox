﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class ExposureSlider : Singleton<ExposureSlider>
{
    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(Callback);
        _slider.value = Camera.main.GetComponent<Tonemapping>().exposureAdjustment/8f;
    }

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void Callback(float value)
    {
        Controller.Instance.SetExposure(value);
    }
}