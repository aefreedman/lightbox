﻿using System.Collections.Generic;
using UnityEngine;

public class LightGroup : MonoBehaviour
{
    public GameObject KeyLight;
    public GameObject FillLight;
    public GameObject HairLight;
    public GameObject KickerLight;
    private List<GameObject> _lights;

    public void Awake()
    {
        _lights = new List<GameObject> {KeyLight, FillLight, HairLight, KickerLight};

    }

    public void Start()
    {
    }

    public void AllOn()
    {
        _lights.ForEach(o => o.SetActive(true));
    }

    public void SetThreePoint()
    {
        AllOn();
        KickerLight.SetActive(false);
    }

    public void SetTemperature(float kelvin)
    {
        _lights.ForEach(light => light.GetComponent<Light>().color = Controller.Instance.BlackBodyColor(kelvin));
    }
}