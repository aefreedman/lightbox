﻿using UnityEngine;
using UnityEngine.UI;

public class AmbientLightSlider : MonoBehaviour
{
    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(Callback);
        _slider.value = RenderSettings.ambientIntensity/8f;
    }

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void Callback(float value)
    {
        RenderSettings.ambientIntensity = value*8f;
    }
}