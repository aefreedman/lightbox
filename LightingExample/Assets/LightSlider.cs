﻿using UnityEngine;
using UnityEngine.UI;

public class LightSlider : MonoBehaviour
{
    public LightControl.WhichLight Light;
    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
        _slider.onValueChanged.AddListener(Callback);
    }

    private void Start()
    {
    }

    private void Update()
    {
    }

    private void Callback(float value)
    {
        Controller.Instance.LightControl.SetIntensity(Light, value);
    }
}