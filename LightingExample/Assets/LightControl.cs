﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LightControl : MonoBehaviour
{
    public enum WhichLight
    {
        Key,
        Fill,
        Hair,
        Kicker
    }

    public Toggle[] Toggle;
    public Slider[] Slider;

    private void Start()
    {
    }

    private void Update()
    {
    }

    public void Refresh()
    {
        var current = Controller.Instance.CurrentLightGroup;
        Toggle[0].isOn = current.KeyLight.gameObject.activeInHierarchy;
        Toggle[1].isOn = current.FillLight.gameObject.activeInHierarchy;
        Toggle[2].isOn = current.HairLight.gameObject.activeInHierarchy;
        Toggle[3].isOn = current.KickerLight.gameObject.activeInHierarchy;
        Slider[0].value = current.KeyLight.GetComponent<Light>().intensity / 8f;
        Slider[1].value = current.FillLight.GetComponent<Light>().intensity / 8f;
        Slider[2].value = current.HairLight.GetComponent<Light>().intensity / 8f;
        Slider[3].value = current.KickerLight.GetComponent<Light>().intensity / 8f;
    }

    public void SetLight(WhichLight which)
    {
        switch (which)
        {
            case WhichLight.Key:
                Controller.Instance.CurrentLightGroup.KeyLight.SetActive(Toggle[0].isOn);
                break;
            case WhichLight.Fill:
                Controller.Instance.CurrentLightGroup.FillLight.SetActive(Toggle[1].isOn);

                break;
            case WhichLight.Hair:
                Controller.Instance.CurrentLightGroup.HairLight.SetActive(Toggle[2].isOn);

                break;
            case WhichLight.Kicker:
                Controller.Instance.CurrentLightGroup.KickerLight.SetActive(Toggle[3].isOn);

                break;
            default:
                throw new ArgumentOutOfRangeException("which", which, null);
        }
    }

    public void SetIntensity(WhichLight forLight, float value)
    {
        var v = value*8f;
        var lg = Controller.Instance.CurrentLightGroup;
        switch (forLight)
        {
            case WhichLight.Key:
                lg.KeyLight.GetComponent<Light>().intensity = v;
                break;
            case WhichLight.Fill:
                lg.FillLight.GetComponent<Light>().intensity = v;
                break;
            case WhichLight.Hair:
                lg.HairLight.GetComponent<Light>().intensity = v;
                break;
            case WhichLight.Kicker:
                lg.KickerLight.GetComponent<Light>().intensity = v;
                break;
            default:
                throw new ArgumentOutOfRangeException("forLight", forLight, null);
        }
    }
}