﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LightToggle : MonoBehaviour {



    public LightControl.WhichLight ForLight;

    public void Awake()
    {
        GetComponent<Toggle>().onValueChanged.AddListener(Callback);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Callback(bool value)
    {
        Controller.Instance.LightControl.SetLight(ForLight);
    }


}
