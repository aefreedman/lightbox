﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class Controller : Singleton<Controller>
{
    public enum Shots
    {
        Close,
        Long
    }

    public GameObject LongCamera;
    public GameObject CloseCamera;

    public float[] ExposureLevels;
    public float[] AmbientLevels;

    public LightGroup StandardLightGroup;
    private List<LightGroup> _lightGroups;

    public LightGroup CurrentLightGroup;
    public Camera CurrentCamera;

    public GameObject[] Models;
    private int _currentModel;

    public GameObject UI;

    public LightControl LightControl;

    public void Awake()
    {
        _lightGroups = new List<LightGroup> {StandardLightGroup};
    }

    private void Start()
    {
        SetLightGroup(StandardLightGroup);
        SetCamera((int)Shots.Close);
        CurrentLightGroup.SetTemperature(6000f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow)) NextModel();
        if (Input.GetKeyDown(KeyCode.P)) UI.SetActive(!UI.activeInHierarchy);
        if (Input.GetKeyDown(KeyCode.S)) Screenshot();
    }

    public void Screenshot()
    {
        var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        //var path = Application.persistentDataPath;
        var file = path + "/screenshot_" + DateTime.Now.Ticks + DateTime.Now.Millisecond + ".png";
        Debug.Log(file);
        ScreenCapture.CaptureScreenshot(file, 2);
    }

    public void SetLightGroup(LightGroup which)
    {
        _lightGroups.ForEach(o => o.gameObject.SetActive(false));
        which.gameObject.SetActive(true);
        which.GetComponent<LightGroup>().SetThreePoint();
        CurrentLightGroup = which;
        LightControl.Refresh();
    }

    public void SetCamera(int which)
    {
        var shots = (Shots) which;
        switch (shots)
        {
            case Shots.Close:
                LongCamera.gameObject.SetActive(false);
                CloseCamera.gameObject.SetActive(true);
                CurrentCamera = CloseCamera.GetComponent<Camera>();
                break;
            case Shots.Long:
                LongCamera.gameObject.SetActive(true);
                CloseCamera.gameObject.SetActive(false);
                CurrentCamera = LongCamera.GetComponent<Camera>();
                break;
            default:
                throw new ArgumentOutOfRangeException("which", which, null);
        }
        ExposureSlider.Instance.GetComponent<Slider>().value =
            CurrentCamera.GetComponent<Tonemapping>().exposureAdjustment/8f;
    }

    public void NextModel()
    {
        Models.ToList().ForEach(o => o.SetActive(false));
        _currentModel ++;
        if (_currentModel >= Models.Length) _currentModel = 0;
        Models[_currentModel].SetActive(true);
    }
    public void PreviousModel()
    {
        Models.ToList().ForEach(o => o.SetActive(false));
        _currentModel --;
        if (_currentModel < 0) _currentModel = Models.Length-1;
        Models[_currentModel].SetActive(true);
    }

    public void SetRotation(float value)
    {
        Models[_currentModel].GetComponent<Rotate>().Speed = value;
    }


    public void SetExposure(float sliderValue)
    {
        var exposureValue = sliderValue*8f;
        Camera.main.GetComponent<Tonemapping>().exposureAdjustment = exposureValue;
    }

    public void SetTemperature(float value)
    {
        CurrentLightGroup.SetTemperature(value);
    }

    public Color BlackBodyColor(double temp)
    {
        float x = (float)(temp / 1000.0);
        float x2 = x * x;
        float x3 = x2 * x;
        float x4 = x3 * x;
        float x5 = x4 * x;

        float R, G, B = 0f;

        // red
        if (temp <= 6600)
            R = 1f;
        else
            R = 0.0002889f * x5 - 0.01258f * x4 + 0.2148f * x3 - 1.776f * x2 + 6.907f * x - 8.723f;

        // green
        if (temp <= 6600)
            G = -4.593e-05f * x5 + 0.001424f * x4 - 0.01489f * x3 + 0.0498f * x2 + 0.1669f * x - 0.1653f;
        else
            G = -1.308e-07f * x5 + 1.745e-05f * x4 - 0.0009116f * x3 + 0.02348f * x2 - 0.3048f * x + 2.159f;

        // blue
        if (temp <= 2000f)
            B = 0f;
        else if (temp < 6600f)
            B = 1.764e-05f * x5 + 0.0003575f * x4 - 0.01554f * x3 + 0.1549f * x2 - 0.3682f * x + 0.2386f;
        else
            B = 1f;

        //return Color.FromScRgb(1f, R, G, B);
        return new Color(R, G, B, 1f);
    }
}