### What is this? ###

* A simple interactive lighting demo
* Version 1.0.0
* Models provided by Jenny Jiao-Hsia

### How do I get set up? ###

* Download and open the LightingExample folder in Unity

### Contacts ###

* Contact aefreedman with any questions